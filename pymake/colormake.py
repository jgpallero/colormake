#!/usr/bin/env python

'''
colormake 1.0.6
---------------

A script to color the output of make,
ripping-off colormake.pl by Bjarni R.
Einarsson, with some small changes and
improvements.

Daniel C. Jones <dcjones@cs.washington.edu>
2011.06.01.20.30
J.L.G. Pallero <jgpallero@gmail.com>
2012.10.18.12.10
J.L.G. Pallero <jgpallero@gmail.com>
2024.06.03.14.55

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

'''

import sys
import re

sys.stdin.reconfigure(errors='ignore')

# Some useful color codes.

col_black =        '\033[30m'
col_red =          '\033[31m'
col_green =        '\033[32m'
col_yellow =       '\033[33m'
col_blue =         '\033[34m'
col_magenta =      '\033[35m'
col_cyan =         '\033[36m'
col_ltgray =       '\033[37m'

col_norm =	       '\033[00m'
col_brighten =     '\033[01m'
col_underline =    '\033[04m'
col_blink = 	   '\033[05m'
col_background =   '\033[07m'

# Colors to print

col_compilation      = col_magenta
col_compilation_last = col_yellow
col_make             = col_cyan
col_filename         = col_blue    + col_brighten
col_warning          = col_green   + col_brighten
col_error            = col_red     + col_brighten
col_others           = col_cyan    + col_brighten
col_default          = col_ltgray
col_normal           = col_norm




# This script works simply by matching regular expressions and wrapping the
# matches in ANSI terminal codes. 'patterns' is an array of (pattern, prefix)
# pairs. If a pattern is matched, the prefix is inserted before the line.

# list of compilers (regular expressions)
compilers = (r'^(g|llvm-g|path|open|uh|t|p|nv|nw|ow|tendra|m|plain|musl-g)?cc',
             r'^(i|sun|pg|l)?cc',
             r'^(open|uh|path|sun|pg)?CC',
             r'^(g|clang|c|llvm-g|llvm-c|xlc|gxlc)\+\+',
             r'^(mkoctfile|clang|ack|vc|c89|c99|icpc|xlc|gxlc|xlC|gxlC)',
             r'^(g|f|sunf|pgf|fort)77',
             r'^(f|openf|uhf|pathf|sunf|pgf|xlf)90',
             r'^(g|f|openf|uhf|pathf|sunf|pgf|xlf)95',
             r'^(g|pg|llvm-g)fortran',
             r'^(xlf|xlf2003|f2003|ifort)',
             r'^(CXX|mcxx|plaincxx)',
             r'^(i|x)(.*mingw32.*)(gcc|g\+\+|c\+\+|gfortran)',
             r'^((libtool: )(compile:|link:))'
            )

# common patterns
filename_pat = r'[A-z0-9_\/\. ]+'
cmp_msg_pat  = filename_pat + r':( In | At |\d+:?\d+:|\d+,)'

patterns = [
            # errors
            (r'((e|E)rror:|\] Error)',                      col_error),
            (r'((c|C)ompilation aborted)',                  col_error),
            (r'((c|C)annot find)',                          col_error),
            (r'((u|U)ndefined reference)',                  col_error),
            (r'((c|C)ommand not found)',                    col_error),
            (r'(PGC-S-)',                                   col_error),
            (r'(compilation completed with severe errors)', col_error),
            (r'(: referencia a)',                           col_error),
            (r'((n|N)o se encontr)',                        col_error),
            (r'((n|N)o se puede encontrar)',                col_error),

            # warnings
            (r'((w|W)arning:|: (w|W)arning)',          col_warning),
            (r'(PGC-W-)',                              col_warning),
            (r'(compilation completed with warnings)', col_warning),
            (r'((a|A)viso:|: (a|A)viso)',              col_warning),

            # make messages
            (r'^make:',         col_make),
            (r'^make\[\d+\]:',  col_make),
            (r'^Making all in', col_make),

            #ar, cpp, ld and other similar orders
            (r'^(llvm-|.*mingw32.*)?(ar|ld|cpp|ranlib)', col_others),
            (r'^(.*mingw32.*)?(windres|windmc)',         col_others),
            (r'^ranlib:',                                col_others),

            #message starting with file and line
            (cmp_msg_pat, col_filename),

            # normal
            (r'', col_default)
           ]

patterns = [(re.compile(pat[0]),pat[1]) for pat in patterns]

def line_add_color(line):
    line_out = ''
    line_compilation = False

    for i in compilers:
        pat = re.compile(i)
        if pat.match(line):
            line_out += col_compilation
            line_compilation = True
            break

    if line_compilation==False:
        for (pat, form) in patterns:
            if pat.search(line):
                line_out += form
                break

    line_out += line
    line_out += col_normal
    return line_out

def line_add_color_compilation(line,last):
    if last==True:
        return col_compilation + line + col_normal
    else:
        return col_compilation_last + line + col_normal


if __name__ == '__main__':
    #identifier
    previous_comp_order = 0
    #scanning lines
    for line in iter(sys.stdin.readline, ''):
        #suppress possible newline at the end of line
        line = line.rstrip()
        #check if the end is the character '\'
        if (len(line)>0) and (line[-1]==chr(92)):
            last_char = True
        else:
            last_char = False
        #check if the previous line was a compiler invocation
        if previous_comp_order==True:
            #force color to compiler invocation
            line_color = line_add_color_compilation(line,last_char)
        else:
            #color
            line_color = line_add_color(line)
        #update last character identifier
        previous_comp_order = last_char
        #newline
        line_color += '\n'
        #writing line
        sys.stdout.write(line_color)
