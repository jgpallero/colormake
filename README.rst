.. -*- coding: utf-8 -*-

.. This file is formatted using reStructuredText in order to be parsed into HTML
.. in https://bitbucket.org/jgpallero/libgeoc/

***************************************
`colormake` - Colorize output of `make`
***************************************

The code in this repository was stolen from:

https://github.com/pagekite/Colormake (PERL version)

and

https://github.com/dcjones/colormake (Python version)

I have slightly modified the code in order to provide more compiler names and,
in the case of the Python version, in order to colorize the `make` output orders
of compilation.

For each version of the code exists one folder that contains the executable
files. You need to copy them to a directory included in the `PATH` variable:

- ``plmake``: PERL version of the program. The file `colormake.pl` contains the
  code to colorize the output of `make`, and the script `plmake` passes the
  output of `make` to `colormake.pl`.
- ``pymake``: Python version of the program. The file `colormake.py` contains
  the code to colorize the output of `make`, and the script `pymake` passes the
  output of `make` to `colormake.py`.

Is recomended to make a simbolic link to `plmake` or `pymake` called `colormake`
or make an `alias` as `alias make=colormake` or `alias make=plmake` or
`alias make=pymake` in order to easy of use the program.

License
-------

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
